<?php

use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'TrackedWebpage',
    ['path' => '/tracked-webpage'],
    function (RouteBuilder $routes) {
        $routes->prefix('api', function($routes) {
            /** @var $routes RouteBuilder */
            $routes->fallbacks(DashedRoute::class);
        });
        $routes->fallbacks(DashedRoute::class);
    }
);
