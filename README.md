## Change plugin's title
`PLUGIN/composer.json`

Change "name" value

Replace All in Path `PLUGIN`

NewTitle → AnyPluginName

new-title → any-plugin-name

### Load plugin
Add this line to `APP/config/bootstrap.php`

`Plugin::load('AnyPluginName', ['routes' => true]);`
### Migrate tables into APP
CakePHP 3+ APP root

`$ bin/cake migrations migrate --plugin AnyPluginName`
### API layer
Prefix `any-plugin-name/api`

In order to send request to `PLUGIN/src/Controller/Api/MyPluginController→getById()`,
use this path:

`any-plugin-name/api/my-plugin/get-by-id`
### Tests
Run tests (Plugin root)

`vendor/bin/phpunit`

In order to use plugin's fixtures in `APP/tests`, add this to `APP/composer.json`
```
    "autoload-dev": {
        "psr-4": {
            ... ,
            "AnyPluginName\\Test\\": "./vendor/philsweb/cakephp-plugin-skeleton/tests",
        }
    },
```

`composer dump-autoload`
### Sniff
Fix (Plugin root)

`./sniff -f` 

Check for conflicts (Plugin root)

`./sniff`

### PLUGIN/config/.env
```
export DB_TEST_HOST = localhost
export DB_TEST_NAME = test
export DB_TEST_USERNAME = root
export DB_TEST_PASSWORD =
```